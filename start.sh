#!/bin/sh
export PATH=/go/bin:$PATH
set -e
set -v

etcd_endpoint="http://$ETCD_NODE:${ETCD_PORT:-4001}"

# Based on start script from tarhelypark/nginx-confd-etcd

until confd -onetime -node "$etcd_endpoint"; do
  echo "[dnsmasq] waiting for configuration to become available"
  sleep 5
done

dnsmasq& # Run in subshell, we don't want this a main process, since it stops docker when killed

echo "[dnsmasq] watching etcd for changes"
exec confd -node "$etcd_endpoint" -watch="true"
