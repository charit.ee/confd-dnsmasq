FROM charitee/alpine-confd
MAINTAINER Vasili Sviridov <vasili@charit.ee>

EXPOSE 53/tcp 53/udp

ADD confd/ /etc/confd

WORKDIR /service
ADD start.sh start.sh
RUN apk --no-cache --update add dnsmasq \
    && rm -rf /var/cache/apk/*

CMD ["/bin/sh", "/service/start.sh"]

