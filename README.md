confd-dnsmasq
=============

Local DNS server to resolve internal services

[![](https://badge.imagelayers.io/charitee/confd-dnsmasq:latest.svg)](https://imagelayers.io/?images=charitee/confd-dnsmasq:latest 'Get your own badge on imagelayers.io')

https://gitlab.com/charit.ee/confd-dnsmasq
